import pandas as pd


def del_empty_line(df: pd.DataFrame) -> pd.DataFrame:
    for column in df:
        df = df[~(df[column]).apply(lambda x: len(x) == 0)]
    return df


def clear_data(df):
    df = del_empty_line(df)
